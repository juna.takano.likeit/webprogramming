<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
	<title>title</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="css/delete.css">

</head>
<body >
<form action="LoginServlet" method="get">
   <div class="alert alert-secondary" role="alert">
       ${userInfo.name}さん<a href="LogoutServlet" class="alert-link"type="submit">ログアウト</a>
   </div>
</form>

    <center><h1>ユーザー削除確認</h1></center>
    <br>
    <div class="text">
    <p>ログインID : </p>
    <p>${delete.login_id}</p>
    <p>を本当に削除してよろしいでしょうか。</p>
    </div>

    <br>
    <div class ="button">
   <form action="UserListServlet" method="get">
   <button type="submit" class="btn btn-secondary">キャンセル</button>
   </form>
   <form action="UserDeleteServlet" method="post">
   <input type="hidden" name="id" value="${delete.id}">
    <button type="submit" class="btn btn-secondary">OK</button>
    </form>
    </div>
    </body>
</html>