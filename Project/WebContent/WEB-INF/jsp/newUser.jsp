<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
	<title>title</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="css/newUser.css">
</head>

<body >
<form action="LoginServlet" method="get">

   <div class="alert alert-secondary" role="alert">
       ${userInfo.name}さん<a href="LogoutServlet" class="alert-link"type="submit">ログアウト</a>
   </div>
</form>
    <center><h1>ユーザー新規登録</h1></center>
    <br>
    <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
    <br>
<div class="input" >
  <form  action="CreateServlet" method="post">
     <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">ログインID</label>
    <div class="col-sm-10">
      <input type="text" name ="login_id" class="form-control" id="" >
    </div>
    </div>

     <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">パスワード</label>
    <div class="col-sm-10">
      <input type="password" name="password" class="form-control" id="inputPassword" >
     </div>
     </div>

     <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">パスワード(確認)</label>
    <div class="col-sm-10">
      <input type="password" name="check" class="form-control" id="inputPassword" >
      </div>
      </div>

     <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">ユーザー名</label>
    <div class="col-sm-10">
      <input type="text"name="name" class="form-control" id="" placeholder=>
      </div>
      </div>

     <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">生年月日</label>
    <div class="col-sm-10">
      <input type="date" name="birth_date"class="form-control" id="" >
     </div>
      </div>
    </div>

    <div class ="button">
    <button type="submit" class="btn btn-secondary btn-lg">登録</button>
    </div>
    </form>
<form action="UserListServlet"method="get">
    <div class="link">
    <button type="submit" class="btn btn-link" >戻る</button>
    </div>
    </form>
</body>
</html>