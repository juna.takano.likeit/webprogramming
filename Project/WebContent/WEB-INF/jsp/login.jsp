<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>title</title>



    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <div>
        <center>
            <font size="10">ログイン画面</font>
        </center>
    </div>
    <br>

<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

    <br>
    <div class="login-area">
        <form class="form-signin" action="LoginServlet" method="post">
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label" class="col text-center">ログインID</label>
                <div class="col-sm-7">
                    <input type="text" name="login_id" class="form-control" id="inputPassword" placeholder="">
                </div>
            </div>

            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label" class="col text-center">パスワード</label>
                <div class="col-sm-7">
                    <input type="password" name="password" class="form-control" id="inputPassword" placeholder="">
                </div>
            </div>


            <br>
            <div class="button">
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">ログイン</button>
            </div>
        </form>
    </div>
</body></html>
