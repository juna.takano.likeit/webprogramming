<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
	<title>title</title>

     <link rel="stylesheet" href="css/uInfo.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body >
<form action="LoginServlet" method="get">
    <div class="alert alert-secondary" role="alert">
       ${userInfo.name}さん<a href="LogoutServlet" class="alert-link"type="submit">ログアウト</a>
    </div>
</form>
 <div class="data">
    <h1 style ="text-align:center">ユーザー情報詳細参照</h1>
<br>
</div>

           <p>${Detail.login_id}</p>
           <p>${Detail.name}</p>
           <p>${Detail.birth_date}</p>
           <p>${Detail.create_date}</p>
           <p>${Detail.update_date}</p>


   <form action="UserListServlet"method=get>
   <div class="link">
    <button type="submit" class="btn btn-link">戻る</button>
    </div>
    </form>

</body>
</html>