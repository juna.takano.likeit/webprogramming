<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>title</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="css/index.css">

</head>

<body>
	<form action="LoginServlet" method="get">
     <div class="alert alert-secondary" role="alert" >${userInfo.name}さん<a href="LogoutServlet" class="alert-link" type="submit">ログアウト</a>
     </div>
    </form>
    <center>
        <font size="10">ユーザー一覧</font>
    </center>

    <br>
   <form action="CreateServlet" method=get>
      <div class="link">
        <button type="submit" class="btn btn-link">新規登録</button>
      </div>
    </form>


<form action="UserListServlet" method="post">
    <div class="input">
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label" class="col text-center">ユーザーID</label>
                <div class="col-sm-7">
                    <input type="text" name="login_id"class="form-control" id="">
            </div>
            </div>

            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label" class="col text-center">ユーザー名</label>
                <div class="col-sm-7">
                    <input type="text" name="name" class="form-control" id="">
                </div>
            </div>

    </div>

    <div class="date row">
            <div class="form-group mb-2">
                <label for="staticEmail2" class="sr-only"></label>
                <input type="text" readonly class="form-control-plaintext" id="staticEmail2" value="生年月日">
            </div>
            <div class="form-group mx-sm-3 mb-2">
                <label for="inputPassword2" class="sr-only">年月日</label>
                <input type="date" name="startDate" class="form-control" id="inputPassword2" value="年/月/日">
            </div> ～
            <div class="form-group mx-sm-3 mb-2">
                <label for="inputPassword2" class="sr-only">年月日</label>
                <input type="date" name="endDate" class="form-control" id="inputPassword2" value="年/月/日">
            </div>

    </div>

    <br>
    <div class="button">
         <button type="submit" value="検索" class="btn btn-primary form-submit">検索</button>
    </div>
</form>

     <div class="table-responsive">
             <table class="table table-striped">
               <thead>
                 <tr>
                   <th>ログインID</th>
                   <th>ユーザ名</th>
                   <th>生年月日</th>
                   <th></th>
                 </tr>
               </thead>
               <tbody>
                 <c:forEach var="user" items="${userList}" >
                   <tr>
                     <td>${user.login_id}</td>
                     <td>${user.name}</td>
                     <td>${user.birth_date}</td>
                     <!-- TODO 未実装；ログインボタンの表示制御を行う -->
                     <td>
                       <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>

				<c:if test="${userInfo.login_id == 'admin' || userInfo.login_id == user.login_id}">
                       <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
				</c:if>

	            <c:if test="${userInfo.login_id == 'admin'}">
                       <a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
			    </c:if>


                     </td>
                   </tr>
                 </c:forEach>
               </tbody>
             </table>
           </div>
         </div>
</body></html>
